# BookLendingApp

BookMoths - from book lovers to book lovers.
Our app allows you to keep track of your books online.
We've all stood in front of that book shelf in a shop, eyeing something interesting and oddly familiar while hesitation sinks in: did I buy it the last time?
Hesitate no more - all you gotta do is check your virtual shelf in BookMoths and walk away from that book store with confidence!


## Installation
Install the necessary requirements:

```console
pip install -r requirements.txt
```

## Registrating
The app asks for minimal personal information upon registration and this data will not be shared with any external platforms.

## Logging in
Choose a user name and a password and start maintaining your virtual bookshelf.

## Adding books
Books can be added manually from button "add book". Books already in the system can be added from each book's card (when logged in) by clicking on button "add to my shelf".

## Removing books
Sometimes you give a book away. Or forget where you put it. Or whom you borrowed it to...
A book can be removed from your shelf from button "remove".


BookMoths©
All rights reserved
