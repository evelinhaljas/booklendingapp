from django.urls import path
from . import views
from .views import HomePage


urlpatterns = [
        path('', HomePage.as_view(), name="home"),
        path('books', views.BooksListView.as_view(), name='books'),
        path('create', views.BookCreateView.as_view(), name="create"),
        path('book/<int:pk>', views.BookDetailView.as_view(), name="detail"),
        path('book/<int:pk>/update', views.BookUpdateView.as_view(), name="update"),
        path('book/<int:pk>/assign', views.BookAssignView.as_view(), name="assign"),
        path('book/<int:pk>/assign_save', views.assign_book, name="assign_save"),
        path('book/<int:pk>/remove', views.BookDeleteView.as_view(), name="remove"),
        path('book/<int:pk>/remove_book', views.remove_book, name="remove_book"),
        path('books/search', views.Search.as_view(), name='search'),
        ]
