from django.apps import AppConfig


class BooklendConfig(AppConfig):
    name = 'booklend'
