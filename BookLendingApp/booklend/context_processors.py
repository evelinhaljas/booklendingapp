from .models import Genre


def category_header(request):
    return {'genres': Genre.objects.all()}
