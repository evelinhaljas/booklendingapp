from django.db import models
from django.contrib.auth.models import User


class Genre(models.Model):
    name = models.CharField(max_length=35)

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=40)
    author = models.CharField(max_length=30)
    ISBN = models.IntegerField(unique=True)
    description = models.TextField(blank=True)
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE)
    user_creator = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return f"{self.title}, {self.author} ({self.ISBN})"


