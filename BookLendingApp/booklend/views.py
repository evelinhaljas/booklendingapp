from django.shortcuts import render, redirect
from .models import Book, Genre
from users.models import Profile
from django.views.generic import (
    ListView,
    TemplateView,
    CreateView,
    DetailView,
    UpdateView,
    DeleteView,
    FormView,
)
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Q


class Search(ListView):
    model = Book
    search_fields = ['title', 'author', 'genre']
    template_name = 'booklend/book_filter.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        if not query:
            query = ''
        object_list = Book.objects.filter(
            Q(title__icontains=query) | Q(author__icontains=query) | Q(genre__name__icontains=query) | Q(
                description__icontains=query)
        )
        return object_list


class HomePage(ListView):
    page_title = 'BookMoths'
    model = Book
    template_name = 'booklend/home.html'
    context_object_name = 'books'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        display_books = Book.objects.all().order_by('?')[:9]
        context["display_books"] = display_books
        return context


class BooksListView(ListView):
    page_title = 'BookMoths'
    model = Book
    template_name = 'booklend/books.html'
    context_object_name = 'books'

    def get_queryset(self):

        books = Book.objects.all()

        query = self.request.GET.get('q')

        if query is not None and query is not '':
            books = Book.objects.filter(
                Q(title__icontains=query) | Q(author__icontains=query) | Q(genre__name__icontains=query) | Q(
                    description__icontains=query)
            )

        genre_id = self.request.GET.get('genre')

        if genre_id is not None and genre_id is not '':
            books = books.filter(genre__id=genre_id)

        return books

    def get_context_data(self, **kwargs):
        query = self.request.GET.get('q')
        genre_id = self.request.GET.get('genre')
        context = super().get_context_data(**kwargs)

        if query is not None:
            context['q'] = query
        if genre_id is not None:
            context['genre_id'] = genre_id

        return context


class BookCreateView(LoginRequiredMixin, CreateView):
    page_title = 'BookMoths - Add book'
    model = Book
    fields = ['title', 'author', 'ISBN', 'description', 'genre']
    success_url = '/'

    def form_valid(self, form):
        profile = Profile.objects.get(user=self.request.user)
        response = super().form_valid(form)
        profile.books.add(self.object)
        return response


class BookDetailView(DetailView):
    model = Book
    template_name = 'booklend/book_detail.html'
    context_object_name = 'book'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.owner:
            return True
        return False


class BookUpdateView(LoginRequiredMixin, UpdateView):
    page_title = 'BookMoths - Update post'
    model = Book
    fields = ['title', 'author', 'description', 'ISBN', 'genre']
    success_url = reverse_lazy("books")

    def form_valid(self, form):
        return super().form_valid(form)


class BookDeleteView(DetailView):
    model = Book
    success_url = '/'
    template_name = "booklend/book_confirm_remove.html"


def remove_book(request, pk):
    book = Book.objects.get(id=pk)
    profile = Profile.objects.get(user=request.user)
    profile.books.remove(book)
    return redirect('/myshelf')


class BookAssignView(DetailView):
    model = Book
    # success_url = 'myshelf/'
    template_name = 'booklend/assign.html'


def assign_book(request, pk):
    profile = Profile.objects.get(user=request.user)
    book = Book.objects.get(id=pk)
    profile.books.add(book)
    return redirect('/myshelf')


class UserBooks:
    model = Profile, Book
    template_name = 'users/my_shelf.html'


def users_books():
    user = Profile.objects.all()
    user_books = user.books.values_list('pk', flat=True)
    not_user_books = Book.objects.exlude(pk__in=user_books)
    books = Profile.objects.exlude(books__in=not_user_books)
    return books


