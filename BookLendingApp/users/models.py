from django.db import models
from django.contrib.auth.models import User
from PIL import Image

from booklend.models import Book
from .countries import CHOICES


class Location(models.Model):
    city = models.CharField(max_length=30)
    country = models.CharField(max_length=50, choices=CHOICES)

    def __str__(self):
        return f"{self.country}, {self.city}"


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    img = models.ImageField(default='profile_pic.jpg')
    books = models.ManyToManyField(Book, blank=True)
    location = models.ForeignKey(Location, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, *args, **kwargs):
        super().save()

        # img = Image.open(self.img.path)
        #
        # if img.height > 300 or img.width > 300:
        #     output_size = (300, 300)
        #     img.thumbnail(output_size)
        #     img.save(self.img.path)
